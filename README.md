# check_data_for_mysql


&nbsp;&nbsp;&nbsp;&nbsp;本工具借鉴pt-table-checksum 工具思路改写，可以检查随意两个mysql（支持mysql sql语法的数据库）节点的数据一致性。
> <font color='red'>本工具仅供学习使用，如需检查线上的数据，请充分测试</font>


#### 1、功能介绍

- 检查随意两个节点的数据一致性
- 支持表结构的校验
- 支持并发检查，基于表的并发
- 支持指定时间，可以规避业务高峰期
- 支持网络监控，如果网络超过阈值可以暂停校验
- <font color='red'>不支持无主键(非空唯一键)的表</font>
- <font color='red'>不支持联合主键达到四个字段及以上的表</font>

#### 2、安装教程

##### （1）下载
```
git clone https://gitee.com/mo-shan/check_data_for_mysql.git
cd check_data_for_mysql
```
##### （2）配置
- 编辑配置文件

```
cd /path/check_data_for_mysql
vim conf/check.conf
```
> 请结合实际情况根据注释提示进行相关配置


- 修改工作路径

```
sed -i 's#^work_dir=.*#work_dir=\"/check_data_for_mysql_path\"#g' start.sh #将这里的check_data_for_mysql_path改成check_data_for_mysql的家目录的绝对路径
```

#### 3、使用说明

##### （1）目录介绍

```
moshan /data/git/check_data_for_mysql > tree -L 2
.
├── conf
│   └── check.conf
├── func
│   ├── f_check_diff_for_mysql.sh
│   ├── f_check_diff_for_row.sh
│   ├── f_logging.sh
│   └── f_switch_time.sh
├── log
├── manager.sh
├── README.en.md
├── README.md
└── start.sh

3 directories, 9 files
moshan /data/git/check_data_for_mysql >
```
- conf 配置文件的目录，check.conf 是配置文件
- log 日志目录
- start.sh 主程序
- manager.sh 网络监控脚本，任务状态的管理脚本

##### （2）帮助手册

- 主程序
```
moshan /data/git/check_data_for_mysql > bash start.sh
Usage: start.sh

                 [ -t check_table ]             需要检查的表列表, 默认整库
                                                db.table : 表示是仅检查这个库下的这个表, 其他库的这个表就不检查
                                                table : 表示所有库下的这个表都检查

                 [ -T skip_check_table ]        不需要检查的表, 默认不过滤
                                                db.table : 表示是仅这个库下的这个表不校验, 其他库的这个表会检查
                                                table : 表示所有库下的这个表都不检查

                 [ -d check_db ]                需要检查的库, 默认是除了系统库以外的所有库
                 [ -D skip_check_db ]           不需要检查的库, 默认不过滤
                 [ -w threads ]                 最大并发数, 总共开几个线程进行并行校验

                 [ -l limit_time ]              哪些时间段允许跑校验, 默认是所有时间, 如需限制可以使用该参数进行限制, 多个时间用英文逗号隔开(1,5,10), 时间段可以用中划线连起来(1-5)
                                                1-5,10-15   表示1点到5点(包含1点和5点), 或者10点到15点可以跑, 需要注意都是闭区间的
                                                1,5,10,15   表示1点, 5点, 10点, 15点可以跑

                 [ -f true ]                    是否执行check操作, 默认是false, 只有为true的时候才会check, -f stop 表示终止检查任务
                 [ -c checksize ]               每次从数据库拿多少行数据进行比较, 默认10000, 不建议修改这个值到太大或者太小, 建议值是5000-50000, 较小的话校验时间较长, 较大的话容易影响数据库
                 [ -h ]                         帮助信息

moshan /data/git/check_data_for_mysql >
```
> 可以根据需求进行参数使用，<font color='red'>如需规避业务高峰期在低峰执行校验任务，请使用-l参数指定执行时间 ，如'-l 1-5'表示凌晨1点到5点执行校验任务，如果当天六点前没校验完成，会等到次日凌晨1点继续校验</font>

- 任务管理脚本

```
moshan /data/git/check_data_for_mysql > bash manager.sh -h

Usage: manager.sh

                 [ -a start|stop|continue|pause ]     监控任务的管理动作, 数据校验任务的管理动作
                                                      start : 启动网络监控
                                                      stop|pause|continue : 连同校验脚本一起停掉|暂停|继续

                 [ -t eth0 ]                          网卡设备名, 默认是eth0
                 [ -n 50 ]                            网卡流量超过这个百分比就暂停, 等网卡流量小于这个就继续, 默认是50
                 [ -h ]                               帮助信息


moshan /data/git/check_data_for_mysql > 
```
> 可以根据实际网卡信息针对该网卡进行监控，当流量达到指定的阈值就会暂时暂停数据校验。这个脚本主要是针对使用了中间件，比如mycat（mysql到mycat）。或者是tidb（tikv到tidb），这种情况下回占用较多网络带宽。

##### （3）测试用例-校验通过场景
- <font color='red'>每次执行校验任务的时候强制要清空log目录，所以请做好校验结果得备份</font>
- <font color='red'>执行校验任务的时候强烈建议开启screen</font>
- <font color='red'>如果有网卡监控需求，执行监控脚本时也强烈建议开启screen </font>

先开启一个screen监控网络

```
moshan /data/git/check_data_for_mysql > screen -S check_net_3306
moshan /data/git/check_data_for_mysql > bash manager.sh -a start
[ 2022-01-18 11:55:34 ] [ 1000 Mb/s ] [ RX : 2    MB/S ]  [ TX : 2    MB/S ]
[ 2022-01-18 11:55:35 ] [ 1000 Mb/s ] [ RX : 2    MB/S ]  [ TX : 4    MB/S ]
[ 2022-01-18 11:55:36 ] [ 1000 Mb/s ] [ RX : 2    MB/S ]  [ TX : 2    MB/S ]
[ 2022-01-18 11:55:37 ] [ 1000 Mb/s ] [ RX : 2    MB/S ]  [ TX : 3    MB/S ]
[ 2022-01-18 11:55:38 ] [ 1000 Mb/s ] [ RX : 1    MB/S ]  [ TX : 2    MB/S ]
[ 2022-01-18 11:55:39 ] [ 1000 Mb/s ] [ RX : 1    MB/S ]  [ TX : 2    MB/S ]
[ 2022-01-18 11:55:41 ] [ 1000 Mb/s ] [ RX : 1    MB/S ]  [ TX : 2    MB/S ]
[ 2022-01-18 11:55:42 ] [ 1000 Mb/s ] [ RX : 2    MB/S ]  [ TX : 8    MB/S ]
```

新开启一个screen执行校验任务

```
moshan /data/git/check_data_for_mysql > screen -S check_data_3306
moshan /data/git/check_data_for_mysql > bash start.sh -d dba -t dbatest1 -f true 
[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_prepare:130 ] [ 本次数据一致性检查开始 ]
[ 2022-01-17 20:32:19 ] [ 警告 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:185 ] [ 本次数据一致性检查将检查如下库 : [dba] ]
[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:203 ] [ 正在检查dba库 ]

[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ func/f_check_diff_for_mysql.sh ] [ f_check_diff_for_mysql:249 ] [ dba.dbatest1 ] [ 表结构一致 ]

[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ func/f_check_diff_for_mysql.sh ] [ f_check_diff_for_mysql:491 ] [ dba.dbatest1 ] [ 1,1 ] [ 00 d 00 h 00 m 00 s ] [ 9.09%, (0:0)/1 ] [ 数据一致 ]
[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ func/f_check_diff_for_mysql.sh ] [ f_check_diff_for_mysql:491 ] [ dba.dbatest1 ] [ 2,11 ] [ 00 d 00 h 00 m 00 s ] [ 100.00%, (0:0)/1 ] [ 数据一致 ]
[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ func/f_check_diff_for_mysql.sh ] [ f_check_diff_for_mysql:504 ] [ dba.dbatest1 ] [ 检查完毕 ]

[ 2022-01-17 20:32:19 ] [ 成功 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:242 ] [ 本次数据一致性检查完成 ] [ 通过 ]

moshan /data/git/check_data_for_mysql > 
```
> 检查结束后会提示检查通过，否则就是检查不通过，如下面的用例。

##### （4）测试用例-校验不通过场景
- 执行校验任务的时候强烈建议开启screen
```
moshan /data/git/check_data_for_mysql > screen -S check_data_3306
moshan /data/git/check_data_for_mysql > bash start.sh -d dbatest1 -f true 
[ 2022-01-17 20:32:09 ] [ 成功 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_prepare:130 ] [ 本次数据一致性检查开始 ]
[ 2022-01-17 20:32:09 ] [ 警告 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:185 ] [ 本次数据一致性检查将检查如下库 : [dbatest1] ]
[ 2022-01-17 20:32:09 ] [ 成功 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:203 ] [ 正在检查dbatest1库 ]

[ 2022-01-17 20:32:09 ] [ 警告 ] [ 192.168.1.1 ] [ func/f_check_diff_for_mysql.sh ] [ f_check_diff_for_mysql:242 ] [ dbatest1.dbatest ] [ 表结构不一致 ] [ a_time name ] [ 跳过该表的检查 ]


[ 2022-01-17 20:32:09 ] [ 错误 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:232 ] [ 本次数据一致性检查完成 ] [ 不通过 ]

[ 2022-01-17 20:32:09 ] [ 错误 ] [ 192.168.1.1 ] [ start.sh/start.sh ] [ f_main:237 ] [ dbatest1.dbatest:table structure err ]
moshan /data/git/check_data_for_mysql >
```

> 检查不通过在log目录都会生成一个skip文件, 里面记录了哪些表被跳过检查及跳过原因。

```
moshan /data/git/check_data_for_mysql > ls -l log/skip.log 
-rw-r--r-- 1 root root 37 Jan 17 20:35 log/skip.log
moshan /data/git/check_data_for_mysql > cat log/skip.log 
dbatest1.dbatest:table structure err
moshan /data/git/check_data_for_mysql >
```

