function f_check_diff_for_row()
{
    declare db_name table_name
    db_name="${1}"
    table_name="${2}"
    check_sql_dir="${res_row_dir}/${db_name}"
    diff_sql_dir="${res_diff_dir}/${db_name}"         #细化分析的结果如果还是不一致就会将不一致的部分保存到这目录

    mkdir -p "${check_sql_dir}" "${diff_sql_dir}"

    check_sql_file="${check_sql_dir}/${table_name}.sql"
    error_res_log="${log_dir}/error_exe_mysql"
    awk -F'FROM' '{print "SELECT * FROM "$2}' ${error_log_table_dir}/${table_name}.log > ${check_sql_file}
    diff_sql_file="${diff_sql_dir}/${table_name}.sql" #细化分析的结果如果还是不一致就会将sql保存到这里

    num=1
    while read check_sql
    do
        md5sum_file1="${md5_dir}/${host_list[0]}_${mysql_port1}_${db_name}_${table_name}_${num}.log"
        md5sum_file2="${md5_dir}/${host_list[1]}_${mysql_port2}_${db_name}_${table_name}_${num}.log"
        thread_info="${db_name}.${table_name} ] ["
        [ -f "${stop_file}" ] && break
        read -u 3
        {
            for ((j=0;j<${#host_list[@]};j++))
            do
            {
                if [ "${j}x" == "0x" ]
                then
                    mysql_port="${mysql_port1}"
                else
                    mysql_port="${mysql_port2}"
                fi
                mysql_comm="${mysql_path} -u${mysql_user} -p${mysql_passwd} -h${host_list[${j}]} -P${mysql_port} ${db_name} -A"
                tmp_res_file="${md5_dir}/${host_list[${j}]}_${mysql_port}_${db_name}_${table_name}_${num}.log"
                error_res_log_file="${error_res_log}_${table_name}_${num}"
                for ((t=0;t<=30;t++))
                do
                    { echo "${check_sql}"|timeout 100s ${mysql_comm} -NB > ${tmp_res_file} 2>${error_res_log_file};} && exe_state="${?}" || exe_state="${?}"
                    if [ -s "${tmp_res_file}" ]
                    then
                        break
                    elif [ ${exe_state} -eq 124 ]
                    then
                        echo "echo \"${check_sql}\"|timeout 100s ${mysql_comm} -NB 2>/dev/null"
                        f_logging "$(eval echo ${log_addr}):ERROR" "${thread_info} ${host_list[${j}]}:${mysql_port} ] [ 检查超时, 5s后重试"
                        sleep 5
                    elif [ ${exe_state} -ne 0 ]
                    then
                        f_logging "$(eval echo ${log_addr}):ERROR" "${thread_info} ${host_list[${j}]}:${mysql_port} ] [ ${check_sql} 执行失败 ] [ $(cat ${error_res_log_file})"
                    fi
                done
                [ -f "${error_res_log_file}" ] && rm -f "${error_res_log_file}"
            } &
            done
            wait

            if [ "$(md5sum ${md5sum_file1}|awk '{print $1}')x" == "$(md5sum ${md5sum_file2}|awk '{print $1}')x" ]
            then
                [ -f "${md5sum_file1}" ] && rm -f "${md5sum_file1}"
                [ -f "${md5sum_file2}" ] && rm -f "${md5sum_file2}"
                f_logging "$(eval echo ${log_addr}):INFO" "${thread_info} ${check_sql_file}:${num} ] [ 数据一致"
                echo 1>&3 && continue
            fi

            row_count="$(wc -l < ${md5sum_file1})"

            diff_file="${diff_sql_dir}/${table_name}.${num}.diff"
            diff ${md5sum_file1} ${md5sum_file2} > ${diff_file}

            if [ -s "${diff_file}" ]
            then
                echo "${check_sql}" >> ${diff_sql_file}
                show_info="${thread_info} ${check_sql_file} ] [ 不一致部分请参考这个文件 : ${diff_file} ] [ 数据不一致"
                f_logging "$(eval echo ${log_addr}):ERROR" "${show_info}"
            fi

            [ -f "${md5sum_file1}" ] && rm -f "${md5sum_file1}"
            [ -f "${md5sum_file2}" ] && rm -f "${md5sum_file2}"

            echo 1>&3
        } &
        num=$((${num}+1))
    done < ${check_sql_file}
    wait

    if [ ! -s "${diff_sql_file}" ]
    then
        rm -f "${check_sql_file}" "${diff_sql_file}"
        rmdir "${diff_sql_dir}" 2>/dev/null
    else
        echo "${db_name}.${table_name}:data diff" >> ${skip_file}
    fi
}

