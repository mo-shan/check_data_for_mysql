function f_logging()
{
    #日志函数, 接收三个参数, 第一个表示日志级别, 第二个表示日志内容, 第三个表示是否需要回车(0|1|2,0表示不需要回车,1表示需要一个回车),2表示需要两个回车
    #usage:f_logging "INFO|WARN|ERROR|COMMAND|REMAIND" "This is a log" "-n|NULL" "0|NULL"
    declare fun_name log_mode log_info log_enter enter_opt exit_opt write_log tmp_log
    file_name="$(awk -F: '{print $1}' <<< "${1}"|awk -F/ '{OFS="/";print $(NF-1),$NF}')"    #文件名
    fun_name="$(awk -F: '{print $2}' <<< "${1}")"     #函数名
    line_num="$(awk -F: '{print $3}' <<< "${1}")"     #文件行号
    log_mode="$(awk -F: '{print $4}' <<< "${1}")"     #日志级别
    [ -z "${line_num}" ] && line_num="NULL"           #没指定行号就置为空
    log_info="${2}"     #日志信息
    log_enter="${3}"    #日志行前后是否需要空行
    exit_opt="${4}"     #是否需要退出任务
    write_log="${5}"    #是否需要写到日志, 0表示放弃这个日志
    enter_opt=""        #表示回车的动作
    localhost_ip="$(/sbin/ip a 2>/dev/null|grep inet|grep brd|grep scope|awk '{print $2}'|awk -F'/' '{print $1}'|head -1)" || true
    [ "${log_enter}x" == "execute:1x" ] && return 0
    if [ "${log_mode}x" == "WARNx" ]
    then
        #WARN级别是黄色显示
        echo -en "\033[33m"
        log_mode="警告"
    elif [ "${log_mode}x" == "ERRORx" ]
    then
        #ERROR级别是红色显示
        echo -en "\033[31m"
        log_mode="错误"
    elif [ "${log_mode}x" == "REMAINDx" ]
    then
        #COMMAND级别是蓝色显示
        echo -en ""
        log_mode="提示"
    elif [ "${log_mode}x" == "COMMANDx" ]
    then
        #COMMAND级别是蓝色显示
        echo -en "\033[34m"
        log_mode="命令"
    else
        #INFO级别是绿色显示
        #[ "${print_color}x" == "1x" ] && echo -en "\033[32m" || echo -en ""
        echo -en "\033[32m"
        log_mode="成功"
    fi

    if [ "${log_enter}x" == "0x" ]
    then
        log_enter="-n"
    elif [ "${log_enter}x" == "2x" ]
    then
        log_enter="-e"
        enter_opt="\n"
    else
        #相当于值是1,即这是默认值
        log_enter="-e"
    fi

    tmp_log="[ $(date "+%F %H:%M:%S") ] [ ${log_mode} ] [ ${localhost_ip} ] [ ${file_name} ] [ ${fun_name}:${line_num} ] [ ${log_info} ]"

    if [ "${write_log}x" == "0x" ]
    then #不需要写到文件
        echo ${log_enter} "${enter_opt}${tmp_log}${enter_opt}"
        echo -n
    else
        echo ${log_enter} "${enter_opt}${tmp_log}${enter_opt}"|tee -a ${log_file}
        echo -n|tee -a ${log_file}
    fi

    if [ "${exit_opt}x" == "1x" ]
    then
        exit 1
    fi
    return 0
}

