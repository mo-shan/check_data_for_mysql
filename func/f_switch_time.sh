function f_switch_time()
{
    awk '{ 
        if($1 < 60) {
            d = "00";
            h = "00";
            m = "00";
            s = $1 % 60;
            if(s < 10) {
                s="0"s;
            }
            $1 = d" d "h" h "m" m "s" s ";
            print $0;
            next;
        } else if($1 >= 60 && $1 < 3600) {
            d = "00";
            h = "00";
            m = int($1 / 60);
            if (m < 10) {
                m = "0"m;
            }
            s = $1 % 60;
            if ( s < 10 ) { 
                s = "0"s;
            }
            $1 = d" d "h" h "m" m "s" s ";
            print $0;
            next;
        } else if($1 >= 3600 && $1 < 86400) {
            d = "00";
            h = int($1 / 3600);
            if (h < 10) {
                h = "0"h;
            }
            m = int($1 % 3600 / 60);
            if (m < 10) {
                m = "0"m;
            }
            s = $1 % 60;
            if ( s < 10 ) {
                s = "0"s;
            }
            $1 = d" d "h" h "m" m "s" s ";
            print $0;
            next;
        } else if($1 > 86400) {
            d = int($1 / 86400);
            if (d < 10) {
                d = "0"d;
            } 
            h = int($1 % 86400 / 3600);
            if (h < 10) {
                h = "0"h;
            }
            m = int($1 % 3600 / 60);
            if (m < 10) {
                m = "0"m;
            }
            s = $1 % 60;
            if ( s < 10 ) {
                s = "0"s;
            }
            $1 = d" d "h" h "m" m "s" s ";
            print $0;
        }
    }' <<< "${1}"
}
