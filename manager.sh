#!/bin/bash
# File Name: manager.sh
# Author: moshan
# mail: mo_shan@yeah.net
# Created Time: 2021-12-22 19:46:26
# Function: 校验任务监控脚本, 管理脚本
#########################################################################
work_dir="$(pwd)"
log_dir="${work_dir}/log"
pause_file_check_net="${log_dir}/pause_file_check_net"
pause_file_check_data="${log_dir}/pause_file"
stop_file_check_net="${log_dir}/stop_file_check_net"
stop_file_check_data="${log_dir}/stop_file"

conf_file="${work_dir}/conf/check.conf"

if [ -f "${conf_file}" ]
then
    . ${conf_file}
    mysql_comm="${mysql_path} -u${mysql_user} -p${mysql_passwd} -h${mysql_host1} -P${mysql_port1} ${db_name} -A"
else
    f_logging "$(eval echo ${log_addr}):ERROR" "缺少必要的配置文件[${conf_file}" "2" "1"
fi

net_name="eth0"
per_limit=50

function f_pause()
{
    touch ${pause_file_check_net}
    sleep 1
    if [ -f "${pause_file_check_data}" ]
    then
        chattr +i ${pause_file_check_data}
    else
        touch ${pause_file_check_data}
    fi
}

function f_continue()
{
    [ -f "${pause_file_check_net}" ] && rm -f ${pause_file_check_net}
    [ -f "${pause_file_check_data}" ] && chattr -i ${pause_file_check_data}
    [ -f "${pause_file_check_data}" ] && rm -f ${pause_file_check_data}
}

function f_stop()
{
    [ ! -f "${stop_file_check_data}" ] && touch ${stop_file_check_data}
    sleep 1
    f_continue
    sleep 1
    [ ! -f "${stop_file_check_data}" ] && touch ${stop_file_check_data}
    [ ! -f "${stop_file_check_net}" ] && touch ${stop_file_check_net}
}

function f_check_net()
{
    eth="${1}"
    eth_speed="${2}"
    net_limit="${3}"
    RXpre=$(cat /proc/net/dev | grep ${eth} | tr : " " | awk '{print $2}')
    TXpre=$(cat /proc/net/dev | grep ${eth} | tr : " " | awk '{print $10}')

    sleep 1

    RXnext=$(cat /proc/net/dev | grep ${eth} | tr : " " | awk '{print $2}')
    TXnext=$(cat /proc/net/dev | grep ${eth} | tr : " " | awk '{print $10}')

    RX=$((${RXnext}-${RXpre}))
    TX=$((${TXnext}-${TXpre}))

    RX_tmp=$(echo ${RX} | awk -v net_speed="${eth_speed}" '{print int($1/1048576)}')
    TX_tmp=$(echo ${TX} | awk -v net_speed="${eth_speed}" '{print int($1/1048576)}')

    RX_70=$(echo ${RX} | awk -v net_speed="${eth_speed}" -v net_limit="70" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')
    TX_70=$(echo ${TX} | awk -v net_speed="${eth_speed}" -v net_limit="70" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')
    RX_80=$(echo ${RX} | awk -v net_speed="${eth_speed}" -v net_limit="80" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')
    TX_80=$(echo ${TX} | awk -v net_speed="${eth_speed}" -v net_limit="80" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')

    RX=$(echo ${RX} | awk -v net_speed="${eth_speed}" -v net_limit="${net_limit}" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')
    TX=$(echo ${TX} | awk -v net_speed="${eth_speed}" -v net_limit="${net_limit}" '{print (int($1/1048576/(net_speed/8)*100) > net_limit)}')

    color_rx="\033[32m"
    color_tx="\033[32m"

    [ ${#RX_tmp} -eq 1 ] && str1="  "   && str2="    "
    [ ${#RX_tmp} -eq 2 ] && str1="  "   && str2="   "
    [ ${#RX_tmp} -eq 3 ] && str1="  "   && str2="  "
    [ ${#RX_tmp} -eq 4 ] && str1="  "   && str2=" "
    [ ${#RX_tmp} -eq 5 ] && str1=""     && str2=""

    [ ${#TX_tmp} -eq 1 ] &&  str4="    "
    [ ${#TX_tmp} -eq 2 ] &&  str4="   "
    [ ${#TX_tmp} -eq 3 ] &&  str4="  "
    [ ${#TX_tmp} -eq 4 ] &&  str4=" "
    [ ${#TX_tmp} -eq 5 ] &&  str4=""

    [ "${RX_70}" -eq 1 ] && color_rx="\033[33m"
    [ "${TX_70}" -eq 1 ] && color_tx="\033[33m"

    [ "${RX_80}" -eq 1 ] && color_rx="\033[31m"
    [ "${TX_80}" -eq 1 ] && color_tx="\033[31m"

    echo -e "[ $(date +"%F %T") ] [ ${eth_speed} Mb/s ] \033[35m[ RX : ${color_rx}${RX_tmp}${str2}\033[35mMB/S ]${str1}\033[35m[ TX : ${color_tx}${TX_tmp}\033[35m${str4}MB/S ]\033[0m"

    if [ "${RX}" -eq 1 ] || [ "${TX}" -eq 1 ]
    then
        return 1
    else
        return 0
    fi
}

function f_start()
{
    eth_speed="$(ethtool ${net_name}|grep Speed:|awk '{print $2+0}')"

    [ -f "${stop_file_check_net}" ] && rm "${stop_file_check_net}"

    [ ! -f "start.sh" ] && echo -e "\033[31m\n\n请在数据校验脚本的家目录下执行网络检查脚本!!!\n\n \033[0m" && exit

    while :
    do
        [ -f "${pause_file_check_net}" ] && sleep 1 && continue
        if f_check_net "${net_name}" "${eth_speed}" "${per_limit}"
        then
            [ -f "${pause_file_check_data}" ] && rm -f "${pause_file_check_data}" >/dev/null 2>&1
        else
            touch "${pause_file_check_data}"
        fi
        [ -f "${stop_file_check_net}" ] && break
        f_check_threads_running
    done

    [ -f "${stop_file_check_net}" ] && rm "${stop_file_check_net}"
}

function f_usage()
{
    echo
    echo -e "$(printf %-16s "Usage: \033[35m$0")\033[0m\n"
    echo -e "$(printf %-16s ) \033[32m[ -a start|stop|continue|pause ]     \033[33m监控任务的管理动作, 数据校验任务的管理动作"
    echo -e "$(printf %-16s ) \033[32m                                     \033[33mstart : 启动网络监控"
    echo -e "$(printf %-16s ) \033[32m                                     \033[33mstop|pause|continue : 连同校验脚本一起停掉|暂停|继续"
    echo
    echo -e "$(printf %-16s ) \033[32m[ -t eth0 ]                          \033[33m网卡设备名, 默认是eth0"
    echo -e "$(printf %-16s ) \033[32m[ -n 50 ]                            \033[33m网卡流量超过这个百分比就暂停, 等网卡流量小于这个就继续, 默认是50"
    echo -e "$(printf %-16s ) \033[32m[ -h ]                               \033[33m帮助信息"
    echo
    exit 1
}

function f_check_threads_running()
{
    threads_running="$(${mysql_comm} -NBe "show status like 'Threads_running';" 2>/dev/null|awk '{print $2+0}')"
    [ -z "${threads_running}" ] && return 0

    if [ "${threads_running}" -gt "${max_threads_running}" ]
    then
        [ ! -f "${pause_file_check_data}" ] && touch ${pause_file_check_data}
        chattr +i ${pause_file_check_data}
    else
        [ -f "${pause_file_check_data}" ] && chattr -i ${pause_file_check_data}
    fi
}

while getopts :a:t:n:h OPTION
do
    case "$OPTION" in
        a)
            opt="${OPTARG}"
            ;;
        t)
            net_name="${OPTARG}"
            ;;
        n)
            per_limit="${OPTARG}"
            ;;

        h)f_usage;;
        :)echo -e "\n'-${OPTARG}' : 这个选项需要一个值";f_usage;;
        ?|*)echo -e "\n'-${OPTARG}' : 不识别这个选项.";f_usage;;
    esac
done

function f_main()
{
    [ "${opt}x" == "continuex" ] && f_continue
    [ "${opt}x" == "pausex" ] && f_pause
    [ "${opt}x" == "stopx" ] && f_stop
    [ "${opt}x" == "startx" ] && f_start
}

f_main
